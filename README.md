## Sessionization. Python script for making Pandas DataFrame with random customers, products, timestamps, determining sessions, sessions uid

### Goal

1. To create Pandas DataFrame with required conditions (random customer_id, product_id, timestamps)

*Views of the customers aren't separated by sessions

2. Basing on the duration of 3 minutes determining sessions, add sessions to the DataFrame, add unique sessions uid

*Basic DataFrame could be big - up to 100mln rows.

### Files

`sessionization_pandas_df.py` - Python generating Pandas DataFrame with random customer_id, products_id, timestamps, determining sessions (basing on adjacent views between which no more than 3 minutes), generating unique sessions uids

### Packages used

- `pandas` working with DateFrame

- `pprint` representing script results

- `random` generating random DataFrame contents (customer_id, product_id, timestamps)

- `time` generating timestamps

- `tdqm.notebook` progress bar decoration

### Environment

Was tested on OS Ubuntu 22.04, terminal 

Python 3.10.6

### Running instructions

1. Open a new terminal window `CTRL + ALT + T`
2. Run command: 

```
cd [to the script containing directory]
```
3. 

```
python3 sessionization_pandas_df.py 
```
