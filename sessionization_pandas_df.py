import pandas as pd
import pprint as pp
from random import randint
import time
from tqdm.notebook import tqdm
tqdm.pandas()


def random_cs_pd_ts() -> pd.DataFrame:  # Generating DataFrame of random customer_id, product_id, timestamp
    listed_df = []
    current_ts = int(time.time())
    yesterday_ts = int(current_ts - 86400)
    for ts in range(yesterday_ts, current_ts, 1):  # Iterate over all seconds in the day.
        for _ in range(randint(0, 25)):  # That way we have multiple users per second.
            row = {
                "customer_id": randint(1, 100000),
                "product_id": randint(1, 500),
                "timestamp": ts
            }
            listed_df.append(row)
    return pd.DataFrame(listed_df)


df = random_cs_pd_ts()
customer_stats = df.groupby("customer_id")["product_id"].agg(['mean', 'count'])
dv = customer_stats.sort_values('count', ascending=False).head()
bigc_ratings = df[df["customer_id"] == 94268]
bigc_ratings['timestamp'].agg(['min', 'max'])
bigc_ratings = bigc_ratings.sort_values("timestamp")
bigc_ratings['gap'] = bigc_ratings['timestamp'].diff()
bigc_ratings['new_session'] = bigc_ratings['gap'] >= 180
bigc_ratings['session_id'] = bigc_ratings['new_session'].cumsum()


def sessioning(dataframe):
    gap = dataframe["timestamp"].diff()
    new_s = gap >= 180
    return new_s.cumsum()


sessioning(bigc_ratings)
df.sort_values("timestamp", inplace=True)
df.reset_index(inplace=True, drop=True)
sessions = df.groupby("customer_id").progress_apply(sessioning)
df_sessions = df.join(sessions.reset_index(level=0, drop=True).to_frame("session_id"))
df_sessions["sessions_uid"] = df_sessions["customer_id"].astype(str) + "_" + df_sessions['session_id'].astype(str)
pp.pprint(df_sessions)
